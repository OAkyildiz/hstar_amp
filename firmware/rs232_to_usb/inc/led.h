
#ifndef __LED_H__
#define __LED_H__
#include "global.h"

void led_init(void);
__STATIC_INLINE void led_on(void){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_SET); }
__STATIC_INLINE void led_off(void){ HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_RESET); }
__STATIC_INLINE void led_toggle(void){ HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_3); }
#endif /*__LED_H__*/

