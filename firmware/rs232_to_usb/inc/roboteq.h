
#ifndef __USART_ROBOTEQ_H__
#define __USART_ROBOTEQ_H__

#include "stm32l4xx_hal.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_usart.h"
#include <stdarg.h> 
#define PRINTF_BUFFER_SIZE 128
#define TX_BUFFER_SIZE 128
#define RX_BUFFER_SIZE 128

void roboteq_init(void);
void roboteq_putc(uint8_t c);
int roboteq_async_getc(uint8_t *c);
uint8_t roboteq_sync_getc();
void roboteq_printf(const char* format, ...);
void roboteq_txe_int_handler();
void roboteq_rxne_int_handler();
void roboteq_error_int_handler();

#endif /*__USART_ROBOTEQ_H__*/