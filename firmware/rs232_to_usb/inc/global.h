// Some global handlers and includes

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include "stm32l4xx_hal.h"
#include "stm32l4xx_nucleo.h"
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_pwr.h"
#include "stm32l4xx_ll_system.h"
#include "stm32l4xx_ll_utils.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_tim.h"

#include "console.h"
#include "timer.h"
#include "led.h"
#include "roboteq.h"

void system_init(void);
inline static void wait_for_interrupt(void ){ HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI); }

#endif /*__GLOBAL_H__*/
