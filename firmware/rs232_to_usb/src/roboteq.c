//roboteq.c
// TODO: implement interrupt-based transmitting with a buffer+locks, so that there's no busy-wait on TX
#include "global.h"
#include "roboteq.h"

uint8_t g_usart1_output_buffer[TX_BUFFER_SIZE]; // circular buffer for TX
volatile uint16_t g_usart1_output_buffer_put_idx, g_usart1_output_buffer_get_idx;
uint8_t g_usart1_input_buffer[RX_BUFFER_SIZE]; // circular buffer for RX
volatile uint16_t g_usart1_input_buffer_put_idx, g_usart1_input_buffer_get_idx;

void roboteq_init(void){
    for(int i = 0; i < TX_BUFFER_SIZE; i++) g_usart1_output_buffer[i] = 0;
    g_usart1_output_buffer_put_idx = 0;
    g_usart1_output_buffer_get_idx = 0;
    for(int i = 0; i < RX_BUFFER_SIZE; i++) g_usart1_input_buffer[i] = 0;
    g_usart1_input_buffer_put_idx = 0;
    g_usart1_input_buffer_get_idx = 0;
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_USART1_CLK_ENABLE();
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_10, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_8_15(GPIOA, LL_GPIO_PIN_9, LL_GPIO_AF_7);
    LL_GPIO_SetAFPin_8_15(GPIOA, LL_GPIO_PIN_10, LL_GPIO_AF_7);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_10, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_10, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_10, LL_GPIO_PULL_UP);
    LL_USART_SetTransferDirection(USART1, LL_USART_DIRECTION_TX_RX);
    LL_USART_ConfigCharacter(USART1, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);
    LL_USART_SetBaudRate(USART1, SystemCoreClock, LL_USART_OVERSAMPLING_16, 115200); 
    LL_USART_Enable(USART1);
    while((!(LL_USART_IsActiveFlag_TEACK(USART1))) || (!(LL_USART_IsActiveFlag_REACK(USART1))));
    LL_USART_EnableIT_RXNE(USART1);
    LL_USART_EnableIT_ERROR(USART1);
    HAL_NVIC_SetPriority(USART1_IRQn,3,4);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
}

void roboteq_putc(uint8_t c){
    //while(!LL_USART_IsActiveFlag_TXE(USART1));
    //LL_USART_TransmitData8(USART1, c);
    if((g_usart1_output_buffer_put_idx == g_usart1_output_buffer_get_idx) && LL_USART_IsActiveFlag_TXE(USART1)){ // free to write straight to transmit (no buffer)
        LL_USART_TransmitData8(USART1, c);
    }
    else if(g_usart1_output_buffer_get_idx != ((g_usart1_output_buffer_put_idx+1)%TX_BUFFER_SIZE)){ // make sure the buffer's not full
        g_usart1_output_buffer[g_usart1_output_buffer_put_idx] = c;
        g_usart1_output_buffer_put_idx = (g_usart1_output_buffer_put_idx+1)%TX_BUFFER_SIZE;
        LL_USART_EnableIT_TXE(USART1);
    }
    else{
        // do something to handler a full buffer?
    }
}
int roboteq_async_getc(uint8_t *c){
    if(g_usart1_input_buffer_put_idx == g_usart1_input_buffer_get_idx){
        return 0;
    }
    else{
        *c = g_usart1_input_buffer[g_usart1_input_buffer_get_idx];
        g_usart1_input_buffer_get_idx = (g_usart1_input_buffer_get_idx+1)%RX_BUFFER_SIZE;
        return 1;
    }
}
uint8_t roboteq_sync_getc(){
    while(g_usart1_input_buffer_put_idx == g_usart1_input_buffer_get_idx);
    uint8_t c = g_usart1_input_buffer[g_usart1_input_buffer_get_idx];
    g_usart1_input_buffer_get_idx = (g_usart1_input_buffer_get_idx+1)%RX_BUFFER_SIZE;
    return c;
}
void roboteq_printf(const char* format, ...){
    char buffer[PRINTF_BUFFER_SIZE];
    int length;
    va_list arg_list;
    va_start(arg_list, format);
    length = vsprintf(buffer, format, arg_list);
    va_end(arg_list);
    if(length > PRINTF_BUFFER_SIZE){
        length = sprintf(buffer, "Buffer overflow on printf\r\n");
    }
    for(uint16_t i=0; i < length; i++) roboteq_putc(buffer[i]);
}
void roboteq_txe_int_handler(){
    if(LL_USART_IsActiveFlag_TXE(USART1) && (g_usart1_output_buffer_put_idx != g_usart1_output_buffer_get_idx)){ // make sure the buffer's not empty and TXE is high
        LL_USART_TransmitData8(USART1, g_usart1_output_buffer[g_usart1_output_buffer_get_idx]);
        g_usart1_output_buffer_get_idx = (g_usart1_output_buffer_get_idx+1)%TX_BUFFER_SIZE;
    } 
    if(g_usart1_output_buffer_put_idx == g_usart1_output_buffer_get_idx){ // buffer empty
        LL_USART_DisableIT_TXE(USART1);
    }
}
void roboteq_rxne_int_handler(){
    if(LL_USART_IsActiveFlag_RXNE(USART1)) { // make sure RXNE is high
        if(g_usart1_input_buffer_get_idx != ((g_usart1_input_buffer_put_idx+1)%RX_BUFFER_SIZE)) //// make sure the buffer's not full
        {
            __IO uint32_t received_char = LL_USART_ReceiveData8(USART1);
            g_usart1_input_buffer[g_usart1_input_buffer_put_idx] = received_char;
            g_usart1_input_buffer_put_idx = (g_usart1_input_buffer_put_idx+1)%RX_BUFFER_SIZE;
        }
        else{
            // do something to handler a full buffer?
        }
    } 
}

void roboteq_error_int_handler(){
    if(LL_USART_IsActiveFlag_FE(USART1)) { // Framing error
        SET_BIT(USART1->ICR, USART_ICR_FECF);
    } 
    if(LL_USART_IsActiveFlag_NE(USART1)) { // Noise error
        SET_BIT(USART1->ICR, USART_ICR_NCF);
    } 
    if(LL_USART_IsActiveFlag_ORE(USART1)) { // Overrun error
        SET_BIT(USART1->ICR, USART_ICR_ORECF);
    }
}
