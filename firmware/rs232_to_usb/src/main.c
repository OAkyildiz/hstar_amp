
#include "global.h"

int main() {
    system_init();
    led_init();
    console_init();
    roboteq_init();
    timer_init();
    wait_for_interrupt();
}
