// Some global handlers and includes

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include "stm32l4xx_hal.h"
#include "stm32l4xx_nucleo.h"
#include "stm32l4xx_ll_bus.h"
#include "stm32l4xx_ll_rcc.h"
#include "stm32l4xx_ll_pwr.h"
#include "stm32l4xx_ll_system.h"
#include "stm32l4xx_ll_utils.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_tim.h"

#include "console.h"
#include "timer.h"
#include "led.h"
#include "lcd.h"
#include "adc.h"

#define MAX_PACKET_SIZE 6
#define START_CHARACTER 0x7E
#define TERMINATION_CHARACTER 0x81

#define ADC_VOLTAGE_RANGE 3.3 // 3.3v ADC
#define ADC_SAMPLE_RANGE 4095.0 // 12-bit
#define VOLATAGE_RATIO 11.0 // voltage divider on power board
#define LPF_ALPHA .99 // low-pass filter
#define BATTERY_MAX 26.1 // approx full charge voltage for 24V pb-acid
#define BATTERY_MIN 23.5 // approx empty charge voltage for 24V pb-acid

#define CLAMP(a,b,c) ((a<b)?b:((a>c)?c:a))

typedef union{
  uint8_t raw[MAX_PACKET_SIZE];
  struct __attribute__((__packed__)){
    uint8_t start;
    float voltage;
    uint8_t end;
  }packet;
} DataPacket;
DataPacket g_feedback_buffer;

void system_init(void);
inline static void wait_for_interrupt(void ){ HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI); }

#endif /*__GLOBAL_H__*/
