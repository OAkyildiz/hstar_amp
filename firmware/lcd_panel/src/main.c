
#include "global.h"

int main() {
    system_init();

    g_feedback_buffer.packet.start = START_CHARACTER;
    g_feedback_buffer.packet.end = TERMINATION_CHARACTER;
    
    led_init();
    console_init();
    lcd_init();
    adc_init();
    timer_init();
    
    wait_for_interrupt();
}
