//console.c
// TODO: implement interrupt-based transmitting with a buffer+locks, so that there's no busy-wait on TX
#include "global.h"
#include "console.h"

uint8_t g_usart2_output_buffer[TX_BUFFER_SIZE]; // circular buffer for TX
volatile uint16_t g_usart2_output_buffer_put_idx, g_usart2_output_buffer_get_idx;
uint8_t g_usart2_input_buffer[RX_BUFFER_SIZE]; // circular buffer for RX
volatile uint16_t g_usart2_input_buffer_put_idx, g_usart2_input_buffer_get_idx;

void console_init(void){
    for(int i = 0; i < TX_BUFFER_SIZE; i++) g_usart2_output_buffer[i] = 0;
    g_usart2_output_buffer_put_idx = 0;
    g_usart2_output_buffer_get_idx = 0;
    for(int i = 0; i < RX_BUFFER_SIZE; i++) g_usart2_input_buffer[i] = 0;
    g_usart2_input_buffer_put_idx = 0;
    g_usart2_input_buffer_get_idx = 0;
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_USART2_CLK_ENABLE();
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_2, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_15, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_0_7(GPIOA, LL_GPIO_PIN_2, LL_GPIO_AF_7);
    LL_GPIO_SetAFPin_8_15(GPIOA, LL_GPIO_PIN_15, LL_GPIO_AF_3);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_2, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_15, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_2, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_15, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_2, LL_GPIO_PULL_UP);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_15, LL_GPIO_PULL_UP);
    LL_USART_SetTransferDirection(USART2, LL_USART_DIRECTION_TX_RX);
    LL_USART_ConfigCharacter(USART2, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);
    LL_USART_SetBaudRate(USART2, SystemCoreClock, LL_USART_OVERSAMPLING_16, 115200); 
    LL_USART_Enable(USART2);
    while((!(LL_USART_IsActiveFlag_TEACK(USART2))) || (!(LL_USART_IsActiveFlag_REACK(USART2))));
    LL_USART_EnableIT_RXNE(USART2);
    LL_USART_EnableIT_ERROR(USART2);
    HAL_NVIC_SetPriority(USART2_IRQn,2,3);
    HAL_NVIC_EnableIRQ(USART2_IRQn);
}

void console_putc(uint8_t c){
    //while(!LL_USART_IsActiveFlag_TXE(USART2));
    //LL_USART_TransmitData8(USART2, c);
    if((g_usart2_output_buffer_put_idx == g_usart2_output_buffer_get_idx) && LL_USART_IsActiveFlag_TXE(USART2)){ // free to write straight to transmit (no buffer)
        LL_USART_TransmitData8(USART2, c);
    }
    else if(g_usart2_output_buffer_get_idx != ((g_usart2_output_buffer_put_idx+1)%TX_BUFFER_SIZE)){ // make sure the buffer's not full
        g_usart2_output_buffer[g_usart2_output_buffer_put_idx] = c;
        g_usart2_output_buffer_put_idx = (g_usart2_output_buffer_put_idx+1)%TX_BUFFER_SIZE;
        LL_USART_EnableIT_TXE(USART2);
    }
    else{
        // do something to handler a full buffer?
    }
}
int console_async_getc(uint8_t *c){
    if(g_usart2_input_buffer_put_idx == g_usart2_input_buffer_get_idx){
        return 0;
    }
    else{
        *c = g_usart2_input_buffer[g_usart2_input_buffer_get_idx];
        g_usart2_input_buffer_get_idx = (g_usart2_input_buffer_get_idx+1)%RX_BUFFER_SIZE;
        return 1;
    }
}
uint8_t console_sync_getc(){
    while(g_usart2_input_buffer_put_idx == g_usart2_input_buffer_get_idx);
    uint8_t c = g_usart2_input_buffer[g_usart2_input_buffer_get_idx];
    g_usart2_input_buffer_get_idx = (g_usart2_input_buffer_get_idx+1)%RX_BUFFER_SIZE;
    return c;
}
void console_printf(const char* format, ...){
    char buffer[PRINTF_BUFFER_SIZE];
    int length;
    va_list arg_list;
    va_start(arg_list, format);
    length = vsprintf(buffer, format, arg_list);
    va_end(arg_list);
    if(length > PRINTF_BUFFER_SIZE){
        length = sprintf(buffer, "Buffer overflow on printf\r\n");
    }
    for(uint16_t i=0; i < length; i++) console_putc(buffer[i]);
}
void console_txe_int_handler(){
    if(LL_USART_IsActiveFlag_TXE(USART2) && (g_usart2_output_buffer_put_idx != g_usart2_output_buffer_get_idx)){ // make sure the buffer's not empty and TXE is high
        LL_USART_TransmitData8(USART2, g_usart2_output_buffer[g_usart2_output_buffer_get_idx]);
        g_usart2_output_buffer_get_idx = (g_usart2_output_buffer_get_idx+1)%TX_BUFFER_SIZE;
    } 
    if(g_usart2_output_buffer_put_idx == g_usart2_output_buffer_get_idx){ // buffer empty
        LL_USART_DisableIT_TXE(USART2);
    }
}
void console_rxne_int_handler(){
    if(LL_USART_IsActiveFlag_RXNE(USART2)) { // make sure RXNE is high
        if(g_usart2_input_buffer_get_idx != ((g_usart2_input_buffer_put_idx+1)%RX_BUFFER_SIZE)) //// make sure the buffer's not full
        {
            __IO uint32_t received_char = LL_USART_ReceiveData8(USART2);
            g_usart2_input_buffer[g_usart2_input_buffer_put_idx] = received_char;
            g_usart2_input_buffer_put_idx = (g_usart2_input_buffer_put_idx+1)%RX_BUFFER_SIZE;
        }
        else{
            // do something to handler a full buffer?
        }
    } 
}

void console_error_int_handler(){
    if(LL_USART_IsActiveFlag_FE(USART2)) { // Framing error
        SET_BIT(USART2->ICR, USART_ICR_FECF);
    } 
    if(LL_USART_IsActiveFlag_NE(USART2)) { // Noise error
        SET_BIT(USART2->ICR, USART_ICR_NCF);
    } 
    if(LL_USART_IsActiveFlag_ORE(USART2)) { // Overrun error
        SET_BIT(USART2->ICR, USART_ICR_ORECF);
    }
}
