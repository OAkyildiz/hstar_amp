
#include "global.h"
#include "stm32l4xx_it.h"

void NMI_Handler(void){}
void HardFault_Handler(void) { HAL_NVIC_SystemReset(); }
void MemManage_Handler(void) { HAL_NVIC_SystemReset(); }
void BusFault_Handler(void) { HAL_NVIC_SystemReset(); }
void UsageFault_Handler(void) { HAL_NVIC_SystemReset(); }
void SVC_Handler(void){}
void DebugMon_Handler(void){}
void PendSV_Handler(void){}
void SysTick_Handler(void){ HAL_IncTick(); }

static int connected = 0;
void TIM2_IRQHandler(void)
{
    if(LL_TIM_IsActiveFlag_CC1(TIM2))
    {
        LL_TIM_ClearFlag_CC1(TIM2);
        led_toggle(); // toggle onboard LED at .5hz
        // voltage reporting to computer at 1hz
        g_feedback_buffer.packet.voltage = g_adc_filtered_data[0];
        for(int i = 0; i<MAX_PACKET_SIZE; i++) console_putc(g_feedback_buffer.raw[i]);

        int charge = (int)((g_adc_filtered_data[0]-BATTERY_MIN)/(BATTERY_MAX-BATTERY_MIN)*100.);
        lcd_putc(0xFE); // control character
        lcd_putc(128); // first position
        lcd_printf("Bat: %4.1fV   %2d%%",g_adc_filtered_data[0],CLAMP(charge,0,99));
        if(!connected)
        {
            lcd_putc(0xFE); // control character
            lcd_putc(192); // first position of the second row
            lcd_printf("Status: Booting ");
        }
    }
}

void USART2_IRQHandler(void)
{
    if(LL_USART_IsActiveFlag_RXNE(USART2) && LL_USART_IsEnabledIT_RXNE(USART2))
    {
        console_rxne_int_handler();
        uint8_t c;
        led_toggle();
        connected = 1;
        while(console_async_getc(&c)) {
            lcd_putc(c);
        }
    }
    if(LL_USART_IsActiveFlag_TXE(USART2) && LL_USART_IsEnabledIT_TXE(USART2)){
        console_txe_int_handler();
    }
    if((LL_USART_IsActiveFlag_FE(USART2) || LL_USART_IsActiveFlag_NE(USART2) || LL_USART_IsActiveFlag_ORE(USART2)) && LL_USART_IsEnabledIT_ERROR(USART2)){
        console_error_int_handler();
    }
}

void USART1_IRQHandler(void)
{
    if(LL_USART_IsActiveFlag_RXNE(USART1) && LL_USART_IsEnabledIT_RXNE(USART1))
    {
    }
    if(LL_USART_IsActiveFlag_TXE(USART1) && LL_USART_IsEnabledIT_TXE(USART1)){
        lcd_txe_int_handler();
    }
    if((LL_USART_IsActiveFlag_FE(USART1) || LL_USART_IsActiveFlag_NE(USART1) || LL_USART_IsActiveFlag_ORE(USART1)) && LL_USART_IsEnabledIT_ERROR(USART1)){
        lcd_error_int_handler();
    }
}


void ADC1_2_IRQHandler(void)
{
    HAL_ADC_IRQHandler(&g_adc_handler);
}
void DMA1_Channel1_IRQHandler(void)
{
    HAL_DMA_IRQHandler(g_adc_handler.DMA_Handle);
    for(size_t i = 0; i < ADC_CONVERTED_DATA_BUFFER_SIZE; i++)
    {
        float data = ((float)g_adc_converted_data[i]) * ADC_VOLTAGE_RANGE / ADC_SAMPLE_RANGE * VOLATAGE_RATIO;
        g_adc_filtered_data[i] = (g_adc_filtered_data[i] * LPF_ALPHA) + (data * (1.0-LPF_ALPHA));
    }
}
