//lcd.c
#include "global.h"
#include "lcd.h"

uint8_t g_usart1_output_buffer[TX_BUFFER_SIZE]; // circular buffer for TX
volatile uint16_t g_usart1_output_buffer_put_idx, g_usart1_output_buffer_get_idx;

void lcd_init(void){
    for(int i = 0; i < TX_BUFFER_SIZE; i++) g_usart1_output_buffer[i] = 0;
    g_usart1_output_buffer_put_idx = 0;
    g_usart1_output_buffer_get_idx = 0;
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_USART1_CLK_ENABLE();
    LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
    LL_GPIO_SetAFPin_8_15(GPIOA, LL_GPIO_PIN_9, LL_GPIO_AF_7);
    LL_GPIO_SetPinSpeed(GPIOA, LL_GPIO_PIN_9, LL_GPIO_SPEED_FREQ_HIGH);
    LL_GPIO_SetPinOutputType(GPIOA, LL_GPIO_PIN_9, LL_GPIO_OUTPUT_PUSHPULL);
    LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_9, LL_GPIO_PULL_UP);
    LL_USART_SetTransferDirection(USART1, LL_USART_DIRECTION_TX);
    LL_USART_ConfigCharacter(USART1, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);
    LL_USART_SetBaudRate(USART1, SystemCoreClock, LL_USART_OVERSAMPLING_16, 9600); 
    LL_USART_Enable(USART1);
    while(!(LL_USART_IsActiveFlag_TEACK(USART1)));
    LL_USART_EnableIT_ERROR(USART1);
    HAL_NVIC_SetPriority(USART1_IRQn,3,4);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
}

void lcd_putc(uint8_t c){
    //while(!LL_USART_IsActiveFlag_TXE(USART1));
    //LL_USART_TransmitData8(USART1, c);
    if((g_usart1_output_buffer_put_idx == g_usart1_output_buffer_get_idx) && LL_USART_IsActiveFlag_TXE(USART1)){ // free to write straight to transmit (no buffer)
        LL_USART_TransmitData8(USART1, c);
    }
    else if(g_usart1_output_buffer_get_idx != ((g_usart1_output_buffer_put_idx+1)%TX_BUFFER_SIZE)){ // make sure the buffer's not full
        g_usart1_output_buffer[g_usart1_output_buffer_put_idx] = c;
        g_usart1_output_buffer_put_idx = (g_usart1_output_buffer_put_idx+1)%TX_BUFFER_SIZE;
        LL_USART_EnableIT_TXE(USART1);
    }
    else{
        // do something to handler a full buffer?
    }
}
void lcd_printf(const char* format, ...){
    char buffer[PRINTF_BUFFER_SIZE];
    int length;
    va_list arg_list;
    va_start(arg_list, format);
    length = vsprintf(buffer, format, arg_list);
    va_end(arg_list);
    if(length > PRINTF_BUFFER_SIZE){
        length = sprintf(buffer, "Buffer overflow on printf\r\n");
    }
    for(uint16_t i=0; i < length; i++) lcd_putc(buffer[i]);
}
void lcd_txe_int_handler(){
    if(LL_USART_IsActiveFlag_TXE(USART1) && (g_usart1_output_buffer_put_idx != g_usart1_output_buffer_get_idx)){ // make sure the buffer's not empty and TXE is high
        LL_USART_TransmitData8(USART1, g_usart1_output_buffer[g_usart1_output_buffer_get_idx]);
        g_usart1_output_buffer_get_idx = (g_usart1_output_buffer_get_idx+1)%TX_BUFFER_SIZE;
    } 
    if(g_usart1_output_buffer_put_idx == g_usart1_output_buffer_get_idx){ // buffer empty
        LL_USART_DisableIT_TXE(USART1);
    }
}

void lcd_error_int_handler(){
    if(LL_USART_IsActiveFlag_FE(USART1)) { // Framing error
        SET_BIT(USART1->ICR, USART_ICR_FECF);
    } 
    if(LL_USART_IsActiveFlag_NE(USART1)) { // Noise error
        SET_BIT(USART1->ICR, USART_ICR_NCF);
    } 
    if(LL_USART_IsActiveFlag_ORE(USART1)) { // Overrun error
        SET_BIT(USART1->ICR, USART_ICR_ORECF);
    }
}
